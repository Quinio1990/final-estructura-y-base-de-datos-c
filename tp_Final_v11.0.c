#include<stdio.h>
#include<conio.h>
#include<stdlib.h>
#include<string.h>
#define p printf
#define s scanf

// DECLARACI�N DE STRUCTs (06):

typedef struct
{   int dia;
	int mes;
	int ano;
}fecha;

typedef struct
{   int idCredito;
    int idCli;
    int idVend;
    int idCob;
    int tipo;
    char descProd[30];
    int cantCuotas;
    double montoPedido;
    double montoAPagar;
    double valorCuota;
    double saldo;
    double montoAtraso;
}credito;

typedef struct
{   char nombre[45];
	char apellido[45];
	fecha f_nac;
	int telf;
	char direc[45];
}persona;

typedef struct
{   int idCliente;
	persona datos;
	int monto;
}cliente;

typedef struct
{   int idVendedor;
	persona datos;
	double sueldo;
}vendedor;

typedef struct
{   int idCobrador;
	persona datos;
	double sueldo;
}cobrador;

//DECLARACI�N DE FUNCIONES:
int menu();
void altaPersona(persona *);
void altaCliente(FILE *);
void altaVendedor(FILE *);
void altaCobrador(FILE *);
void altaCredito(FILE *, FILE *, FILE *, FILE *);
int cantClientes(FILE *);
int cantVendedores(FILE *);
int cantCobradores(FILE *);
void consultaClientes(FILE *);
void consultaVendedores(FILE *);
void consultaCobradores(FILE *);
void consultaCreditos(FILE *);
void ingresarFecha(persona*);
int validarFecha(int, int, int);
void cobroDiario(FILE*,FILE*);
void tecla();

/*
**********************************************
*****************    MAIN    *****************
**********************************************
*/

int main()
{
    FILE *clientes;
    FILE *vendedores;
    FILE *cobradores;
    FILE *creditos;


    char archivoClientes[] = "clientes.dat";
    char archivoVendedores[] = "vendedores.dat";
    char archivoCobradores[] = "cobradores.dat";
    char archivoCreditos[] = "creditos.dat";

    if ((clientes=fopen(archivoClientes,"rb"))==NULL)
        clientes=fopen(archivoClientes,"wb+");
    else
        clientes=fopen(archivoClientes,"ab+");

    if ((vendedores=fopen(archivoVendedores,"rb"))==NULL)
        vendedores=fopen(archivoVendedores,"wb+");
    else
        vendedores=fopen(archivoVendedores,"ab+");

    if ((cobradores=fopen(archivoCobradores,"rb"))==NULL)
        cobradores=fopen(archivoCobradores,"wb+");
    else
        cobradores=fopen(archivoCobradores,"ab+");

    if ((creditos=fopen(archivoCreditos,"rb+"))==NULL)
        creditos=fopen(archivoCreditos,"wb+");
    else
        creditos=fopen(archivoCreditos,"rb+");

    int op = menu();

    while(op!=0)
    {
        system("cls");
        switch(op)
        {
            case 1:
                altaCliente(clientes);
                break;
            case 2:
                altaVendedor(vendedores);
                break;
            case 3:
                altaCobrador(cobradores);
                break;
            case 4:
                altaCredito(creditos, clientes, vendedores, cobradores);
                break;
            case 5:
                consultaClientes(clientes);
                tecla();
                break;
            case 6:
                consultaVendedores(vendedores);
                tecla();
                break;
            case 7:
                consultaCobradores(cobradores);
                tecla();
                break;
            case 8:
                consultaCreditos(creditos);
                tecla();
                break;
            case 9:
                cobroDiario(creditos,cobradores);
                tecla();
                break;
            default:
                break;
        }
        op = menu();
    }

    return 0;
}
//***************   FIN MAIN   ***************



/*
**********************************************
**************    FUNCIONES    ***************
**********************************************
*/

int menu()
{
    int op;
    system("cls");
    p("\n========================================================");
    p("\n ==============  EMPRESA DE PAGO DIARIO  ==============");
    p("\n========================================================\n");
    p("\nElija una opci%cn:\n", 162);
    p(" [1] Alta cliente.\n");
    p(" [2] Alta vendedor.\n");
    p(" [3] Alta cobrador.\n");
    p(" [4] Alta cr%cdito.\n", 130);
    p(" [5] Consulta clientes.\n");
    p(" [6] Consulta vendedores.\n");
    p(" [7] Consulta cobradores.\n");
    p(" [8] Consulta cr%cditos.\n", 130);
    p(" [9] Cargar cobro diario.\n");
    p(" [0] SALIR.\n");
    p("OPCI%cN:  ", 224);
    s("%d", &op);

    return op;
}

void altaPersona(persona *pers)//char nom, char ape, int dia, int mes, int ano, int telf, char direc)
{
    int dia, mes, ano;

    p("Nombre: ");
    fflush(stdin);
    gets(pers->nombre);

    p("Apellido: ");
    fflush(stdin);
    gets(pers->apellido);

    p("Fecha de nacimiento: \n");
/*
    p("Ingrese d%ca: ", 161);
    s("%d",&pers->f_nac.dia);
    p("Ingrese mes: ");
    s("%d",&pers->f_nac.mes);
    p("Ingrese a%co: ", 164);
    s("%d",&pers->f_nac.ano);
*/
    ingresarFecha(pers);

    p("Tel%cfono: ", 130);
    s("%d", &pers->telf);

    p("Direcci%cn: ", 162);
    fflush(stdin);
    gets(pers->direc);
}

void altaCliente(FILE *fCliente)
{
    system("cls");

    persona pers;
    cliente cli;
    int ult=cantClientes(fCliente);
    ult++;

    p("DATOS DEL CLIENTE\n\n");
    altaPersona(&pers);

    cli.idCliente=ult;
    strcpy(cli.datos.nombre, pers.nombre);
    strcpy(cli.datos.apellido, pers.apellido);
    cli.datos.f_nac.dia=pers.f_nac.dia;
    cli.datos.f_nac.mes=pers.f_nac.mes;
    cli.datos.f_nac.ano=pers.f_nac.ano;
    cli.datos.telf=pers.telf;
    strcpy(cli.datos.direc, pers.direc);
    cli.monto=0;

    fwrite(&cli,sizeof(cliente),1,fCliente);
}

void altaVendedor(FILE *fVendedor)
{
    system("cls");

    persona pers;
    vendedor ven;
    int ult=cantVendedores(fVendedor);
    ult++;

    p("DATOS DEL VENDEDOR\n\n");
    altaPersona(&pers);

    ven.idVendedor=ult;
    strcpy(ven.datos.nombre, pers.nombre);
    strcpy(ven.datos.apellido, pers.apellido);
    ven.datos.f_nac.dia=pers.f_nac.dia;
    ven.datos.f_nac.mes=pers.f_nac.mes;
    ven.datos.f_nac.ano=pers.f_nac.ano;
    ven.datos.telf=pers.telf;
    strcpy(ven.datos.direc, pers.direc);
    ven.sueldo=25000.0;

    fwrite(&ven,sizeof(vendedor),1,fVendedor);
}


void altaCobrador(FILE *fCobrador)
{   system("cls");

    persona pers;
    cobrador cob;
    int ult=cantCobradores(fCobrador);
    ult++;
    p("DATOS DEL COBRADOR\n\n");

    altaPersona(&pers);

    cob.idCobrador=ult;
    strcpy(cob.datos.nombre, pers.nombre);
    strcpy(cob.datos.apellido, pers.apellido);
    cob.datos.f_nac.dia=pers.f_nac.dia;
    cob.datos.f_nac.mes=pers.f_nac.mes;
    cob.datos.f_nac.ano=pers.f_nac.ano;
    cob.datos.telf=pers.telf;
    strcpy(cob.datos.direc, pers.direc);
    cob.sueldo=25000.0;

    fwrite(&cob, sizeof(cobrador), 1, fCobrador);
}

void altaCredito(FILE *fCredito, FILE *fCliente, FILE *fVendedor, FILE *fCobrador)
{
    system("cls");

    credito cred;
    int ult=0, idAux, tipo, cantCuo;

    rewind(fCredito);

    //ID
    fread(&cred, sizeof(credito), 1, fCredito); // Lee el archivo.
    while(!feof(fCredito))  // Verifica y toma el �ltimo dato de LEGAJO, si no existe empieza en 1.
    {   ult=cred.idCredito;
        fread(&cred, sizeof(credito), 1, fCredito);
    }
    ult++;
    //FIN ID

    cred.idCredito = ult;

    //Identificar Cliente:
    rewind(fCliente);
    consultaClientes(fCliente);
    p("SELECCIONE ID CLIENTE: ");
    s("%d", &idAux);

    //VALIDAR
    while(idAux<1 || idAux>cantClientes(fCliente))
    {
        p("ID fuera de rango.\nIngrese nuevamente el ID: ");
        s("%d", &idAux);
    }
    cred.idCli = idAux;

    //Identificar Vendedor:
    system("cls");
    rewind(fVendedor);
    consultaVendedores(fVendedor);
    p("SELECCIONE ID VENDEDOR: ");
    s("%d", &idAux);
    //VALIDAR
    while(idAux<1 || idAux>cantVendedores(fVendedor))
    {
        p("ID fuera de rango.\nIngrese nuevamente el ID: ");
        s("%d", &idAux);
    }
    cred.idVend = idAux;

    //Identificar Cobrador:
    system("cls");
    rewind(fCobrador);
    consultaCobradores(fCobrador);
    p("SELECCIONE ID COBRADOR: ");
    s("%d", &idAux);
    //VALIDAR
    while(idAux<1 || idAux>cantCobradores(fCobrador))
    {
        p("ID fuera de rango.\nIngrese nuevamente el ID: ");
        s("%d", &idAux);
    }
    cred.idCob = idAux;

    //Ingreso Tipo de Cr�dito:
    system("cls");
    p("Ingrese el tipo de cr�dito solicitado: ");
    p("\n[1] Producto.");
    p("\n[2] Efectivo");
    p("\nOPCION: ");
    s("%d", &tipo);
    while(tipo<1 || tipo>2)
    {
        p("Opcion invalida.\nIngrese nuevamente el tipo: ");
        p("\n[1] Producto.");
        p("\n[2] Efectivo");
        p("\nOPCION: ");
        s("%d", &tipo);
    }

    cred.tipo = tipo;

    switch(tipo)
    {
        case 1:
            p("Descripcion: ");
            fflush(stdin);
            gets(cred.descProd);
            double monto=0.0;
            p("Ingrese el monto final: ");
            s("%lf", &monto);
            cred.montoAPagar=monto;
            cred.montoPedido = cred.montoAPagar;
            cred.cantCuotas = 140;
            cred.valorCuota = cred.montoAPagar/cred.cantCuotas;

            break;

        case 2:

            strcpy(cred.descProd, "Efectivo");
            p("\nIngrese el monto solicitado: ");
            s("%lf", &cred.montoPedido);
            p("\nIngrese cantidad de cuotas: ");
            p("\n[1] 25 dias");
            p("\n[2] 51 dias");
            p("\n[3] 77 dias");
            p("\nOPCION: ");
            s("%d", &cantCuo);
            while(cantCuo<1 && cantCuo>3)
            {
                p("\nOpcion incorrecta.\nIngrese nuevamente la cantidad de cuotas: ");
                s("%d", &cantCuo);
            }
            if(cantCuo==1)
            {
                cred.cantCuotas=25;
                cred.montoAPagar = cred.montoPedido*1.25;
                cred.valorCuota = cred.montoAPagar/cred.cantCuotas;
            }

            if(cantCuo==2)
            {
                cred.cantCuotas=51;
                cred.montoAPagar = cred.montoPedido*1.51;
                cred.valorCuota = cred.montoAPagar/cred.cantCuotas;
            }
            if(cantCuo==3)
            {
                cred.cantCuotas=77;
                cred.montoAPagar = cred.montoPedido*1.77;
                cred.valorCuota = cred.montoAPagar/cred.cantCuotas;
            }
            break;
        default:
            break;
    }

    cred.saldo = cred.montoAPagar;

    fwrite(&cred, sizeof(credito), 1, fCredito);

}

int cantClientes(FILE *fCliente)
{
    cliente cli;
    int ult=0;

    rewind(fCliente);
    fread(&cli, sizeof(cliente), 1, fCliente); // Lee el archivo.
    while(!feof(fCliente))  // Verifica y toma el �ltimo dato de LEGAJO, si no existe empieza en 1.
    {   ult=cli.idCliente;
        fread(&cli, sizeof(cliente), 1, fCliente);
    }
    return ult;
}

int cantVendedores(FILE *fVendedor)
{
    vendedor ven;
    int ult=0;

    rewind(fVendedor);
    fread(&ven, sizeof(vendedor), 1, fVendedor); // Lee el archivo.
    while(!feof(fVendedor))  // Verifica y toma el �ltimo dato de LEGAJO, si no existe empieza en 1.
    {   ult=ven.idVendedor;
        fread(&ven, sizeof(vendedor), 1, fVendedor);
    }

    return ult;
}

int cantCobradores(FILE *fCobrador)
{
    cobrador cob;
    int ult=0;

    rewind(fCobrador);
    fread(&cob, sizeof(cobrador), 1, fCobrador); // Lee el archivo.
    while(!feof(fCobrador))  // Verifica y toma el �ltimo dato de LEGAJO, si no existe empieza en 1.
    {   ult=cob.idCobrador;
        fread(&cob, sizeof(cobrador), 1, fCobrador);
    }

    return ult;
}

void ingresarFecha(persona *pers)
{
    int valido, dx, mx, ax;
    do
    {   p("Ingrese dia: ");
        s("%d",&dx);
        p("Ingrese mes: ");
        s("%d",&mx);
        p("Ingrese anio: ");
        s("%d",&ax);
        valido=validarFecha(dx, mx, ax);

        if(!valido)
            p("Fecha mal ingresada.\nIngrese nuevamente los datos");
        }while (!valido);

        pers->f_nac.dia=dx;
        pers->f_nac.mes=mx;
        pers->f_nac.ano=ax;
}

int validarFecha(int dia, int mes, int ano)
{
    int valido=0;
    if( mes >= 1 && mes <= 12 )
    {   switch( mes )
        {   case 1: case 3: case 5: case 7: case 8: case 10: case 12:
                if( dia >= 1 && dia <= 31 )
                    valido = 1;
                break;
            case 4: case 6: case 9: case 11:
                if( dia >= 1 && dia <= 30 )
                    valido = 1;
                break;
            case 2: //Si el a�o es bisiesto +1 d�a.
                if(ano>=1900 && ano<=2121)
                {   if( (ano%4 == 0) && (ano%100 != 0) || (ano%400 == 0) )
                    {   if( dia >= 1 && dia <= 29 )
                            valido = 1;
                    }else if( dia >= 1 && dia <= 28 )
                        valido = 1;
                }else
                    valido=0;
                break;
            default:
                valido=0;
                break;
        }
    }
    return valido;
}



void consultaClientes(FILE *pMostrar)
{

    cliente cli;
    rewind(pMostrar);
    p("\nPLANILLA DE CLIENTES:\n\n");
    p(" COD.\tNOMBRE\t\tAPELLIDO\tFECHA DE NAC.\tTELEFONO\tDIRECCION\tMONTO\n");
    fread(&cli, sizeof(cliente), 1, pMostrar);
    while(!feof(pMostrar))
    {
        p("  %d\t%-15s\t%-15s\t%d/%d/%d\t%d\t%-15s\t$ %d", cli.idCliente, cli.datos.nombre, cli.datos.apellido, cli.datos.f_nac.dia, cli.datos.f_nac.mes, cli.datos.f_nac.ano, cli.datos.telf, cli.datos.direc, cli.monto);
        p("\n");
        fread(&cli, sizeof(cliente), 1, pMostrar);
    }
}

void consultaVendedores(FILE *pMostrar)
{

    vendedor ven;
    rewind(pMostrar);
    p("\nPLANILLA DE VENDEDORES:\n\n");
    p(" COD.\tNOMBRE\t\tAPELLIDO\tFECHA DE NAC.\tTELEFONO\tDIRECCION\tSUELDO\n");
    fread(&ven, sizeof(vendedor), 1, pMostrar);
    while(!feof(pMostrar))
    {
        p("  %d\t%-15s\t%-15s\t%d/%d/%d\t%d\t%-15s\t$ %d", ven.idVendedor, ven.datos.nombre, ven.datos.apellido, ven.datos.f_nac.dia, ven.datos.f_nac.mes, ven.datos.f_nac.ano, ven.datos.telf, ven.datos.direc, ven.sueldo);
        p("\n");
        fread(&ven, sizeof(vendedor), 1, pMostrar);
    }
}

void consultaCobradores(FILE *pMostrar)
{
    cobrador cob;
    rewind(pMostrar);
    p("\nPLANILLA DE COBRADORES:\n\n");
    p(" COD.\tNOMBRE\t\tAPELLIDO\tFECHA DE NAC.\tTELEFONO\tDIRECCION\tSUELDO\n");
    fread(&cob, sizeof(cobrador), 1, pMostrar);
    while(!feof(pMostrar))
    {
        p("  %d\t%-15s\t%-15s\t%d/%d/%d\t%d\t%-15s\t$ %d", cob.idCobrador, cob.datos.nombre, cob.datos.apellido, cob.datos.f_nac.dia, cob.datos.f_nac.mes, cob.datos.f_nac.ano, cob.datos.telf, cob.datos.direc, cob.sueldo);
        p("\n");
        fread(&cob, sizeof(cobrador), 1, pMostrar);
    }
}


void consultaCreditos(FILE *pMostrar)
{
    credito cred;
    rewind(pMostrar);
    p("\nLISTADO DE CREDITOS:\n\n");

    fread(&cred, sizeof(credito), 1, pMostrar);
    while(!feof(pMostrar))
    {
        p("ID\tid Cli\tid Vend\tid Cob\tTipo\tDescripcion\n");
        p("%d\t%d\t%d\t%d\t%d\t%-15s",cred.idCredito, cred.idCli, cred.idVend, cred.idCob, cred.tipo, cred.descProd);
        p("\nCuotas: %d", cred.cantCuotas);
        p("\nMonto solicitado: $ %.0lf", cred.montoPedido);
        p("\nTotal a pagar: $ %.0lf", cred.montoAPagar);
        p("\nValor de cuota: $ %.0lf", cred.valorCuota);
        p("\nSaldo: $ %.0lf",cred.saldo);
        p("\nAtraso: $ %.0lf",cred.montoAtraso);
        p("\n-----------------------------------\n");
        fread(&cred, sizeof(credito), 1, pMostrar);
    }


}
void cobroDiario(FILE* fCreditos,FILE* fCobradores)
{
    int idAux=0;
    credito cred;
    double pago=0;

    rewind(fCobradores);
    rewind(fCreditos);
    consultaCobradores(fCobradores);
    p("SELECCIONE ID COBRADOR: ");
    s("%d", &idAux);
    //VALIDAR
    while(idAux<1 || idAux>cantCobradores(fCobradores))
    {
        p("ID fuera de rango.\nIngrese nuevamente el ID: ");
        s("%d", &idAux);
    }
    //FIN VALIDAR


    fread(&cred, sizeof(credito), 1, fCreditos);
    while(!feof(fCreditos))
    {
        if (idAux==cred.idCob && cred.saldo>0)
        {
            p("ID\tid Cli\tid Vend\tid Cob\tTipo\tDescripcion\n");
            p("%d\t%d\t%d\t%d\t%d\t%-15s",cred.idCredito, cred.idCli, cred.idVend, cred.idCob, cred.tipo, cred.descProd);
            p("\nCuotas: %d", cred.cantCuotas);
            p("\nMonto solicitado: $ %.0lf", cred.montoPedido);
            p("\nTotal a pagar: $ %.0lf", cred.montoAPagar);
            p("\nValor de cuota: $ %.0lf", cred.valorCuota);
            p("\nSaldo: $ %.0lf",cred.saldo);
            p("\nAtraso: $ %.0lf",cred.montoAtraso);
            p("\n-------------------------------------\n");
            p("Ingrese el pago:");
            s("%lf",&pago);
            cred.saldo=cred.saldo-pago;
            cred.montoAtraso=cred.montoAtraso+(cred.valorCuota-pago);
            //DAR DE BAJA CREDITO SI TERMINO
            fseek(fCreditos,sizeof(credito)*-1L,SEEK_CUR);
            fwrite(&cred, sizeof(credito), 1, fCreditos);
            fseek(fCreditos,0,SEEK_CUR);
        }
        fread(&cred, sizeof(credito), 1, fCreditos);
    }

}

//*************  FIN FUNCIONES  **************


void tecla()
{   p("\n");
    p("\n\nPulse una tecla para ir al MENU...\n");
    getch();
}



